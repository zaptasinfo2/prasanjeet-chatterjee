import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PosHomeComponent } from './pos-home.component';

const routes: Routes = [
  {path:'',component:PosHomeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PosHomeRoutingModule { }
