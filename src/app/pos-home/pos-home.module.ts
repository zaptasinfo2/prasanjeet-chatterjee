import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PosHomeRoutingModule } from './pos-home-routing.module';
import { PosHomeComponent } from './pos-home.component';

@NgModule({
  declarations: [PosHomeComponent],
  imports: [
    CommonModule,
    PosHomeRoutingModule
  ]
})
export class PosHomeModule { }
