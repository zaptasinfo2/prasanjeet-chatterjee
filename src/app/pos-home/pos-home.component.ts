import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../general.service';

@Component({
  selector: 'app-pos-home',
  templateUrl: './pos-home.component.html',
  styleUrls: ['./pos-home.component.css']
})
export class PosHomeComponent implements OnInit {
  arrayData:any;
  date=new Date();
  newDate;
  discoun=0;
  vatt=0;
  showDate;
  quantity:number=0;
  totalPrice:number=0;
  itemName:string;
  price:number=0;
  vat=0;
  i=0;
  finalPrice=0;
  discount=0;
  constructor(private _general:GeneralService) {
    this.newDate=this.date.toString();
    this.showDate=this.newDate.split("G")[0];
   }

  ngOnInit() {
    this.getData();
  }
  getData(){
   this._general.getJsonData().subscribe((res:any)=>{
     this.arrayData=res;
     console.log(this.arrayData);
   },err=>{
     console.log(err);
   })
  }
  removeData(val){
    this.quantity=val;
  }
  calculatePrice(value,name){
    this.i=1;
    this.vatt=10;
    this.discoun=10;
    console.log(this.i);
    this.itemName=name;
    this.price=value;
    this.quantity=this.quantity+1;
    this.totalPrice=(this.totalPrice)+parseFloat(value);
    this.discount=(this.discount)+(10/100)*(this.totalPrice);
    this.vat=(this.vat)+(10/100)*(this.totalPrice);
    this.finalPrice=this.totalPrice+this.vat+this.discount;
    console.log(this.discount);
    console.log(this.totalPrice);
    console.log(this.finalPrice);
  }
  reduceQuantity(){
    if(this.quantity!=0){
      this.quantity=this.quantity-1;
      this.finalPrice=this.finalPrice-this.totalPrice;
      this.price=this.price-this.totalPrice;
    }
  }
  cancelSale(val){
    this.quantity=val;
    this.discount=val;
    this.itemName="";
    this.vat=val;
    this.finalPrice=val;
    this.totalPrice=val;
  }
  completeSale(val){
    if(this.quantity>0){
      this.quantity=val;
      this.itemName="";
      this.finalPrice=val;
      this.totalPrice=val;
      this.discoun=val;
      this.vatt=val;
    alert('Your sale is completed');
    }
    
  }
  increaseQuantity(){
    this.quantity=this.quantity+1;
    this.finalPrice=this.finalPrice+this.totalPrice;
    this.price=Number.parseInt(this.price.toString())+this.totalPrice;
  }
}
