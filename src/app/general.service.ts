import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class GeneralService {
jsonheader;
constructor(private http:HttpClient) {
  //this.jsonheader=new HttpHeaders({'Content-Type':'application/json'})
 }
 getJsonData(){
   return this.http.get('assets/pos.json');
 }
}
